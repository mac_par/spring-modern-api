package com.modern.chpt03.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.modernapi.model.dto.CartDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {CartController.class})
class CartControllerTest {
    private static final String CART_ROOT_URL = "/carts/{customerId}";
    private static final String TEST_CUSTOMER_ID = "test-customer-id";

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getCustomerCart_dummyCartIsReturned() throws Exception {
        //given
        CartDto cart = CartDto.builder().customerId(TEST_CUSTOMER_ID).build();
        String expectedPayloadJson = objectMapper.writeValueAsString(cart);
        //when
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(CART_ROOT_URL, TEST_CUSTOMER_ID).accept(MediaType.APPLICATION_JSON))
                .andDo(print());
        //then
        result.andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(expectedPayloadJson));
    }
}