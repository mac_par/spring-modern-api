package com.modern.chpt03.controller.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

@Valid
@Slf4j
@ControllerAdvice
public class RestAPIErrorHandler extends ResponseEntityExceptionHandler {
    private final MessageSource messageSource;
    private final HttpServletRequest httpServletRequest;

    @Autowired
    public RestAPIErrorHandler(MessageSource messageSource, HttpServletRequest httpServletRequest) {
        this.messageSource = messageSource;
        this.httpServletRequest = httpServletRequest;
    }

    @ExceptionHandler({Exception.class})
    ResponseEntity<AppError> handleGeneralException(Exception exception, HttpServletRequest request, Locale locale) {

        logErrorMessage("General Error handling", request, exception, locale);
        HttpStatus responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        AppError error = new AppError(ErrorCodes.GENERIC_ERROR, responseStatus, request);
        return ResponseEntity.status(responseStatus).body(error);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logErrorMessage("Type not supported", httpServletRequest, ex, request.getLocale());
        HttpStatus responseStatus = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
        AppError error = new AppError(ErrorCodes.HTTP_MEDIA_TYPE_NOT_SUPPORTED, responseStatus, httpServletRequest);
        return ResponseEntity.status(responseStatus).body(error);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logErrorMessage("Http message not writable", httpServletRequest, ex, httpServletRequest.getLocale());
        AppError error = new AppError(ErrorCodes.HTTP_MESSAGE_NOT_WRITABLE, HttpStatus.UNSUPPORTED_MEDIA_TYPE, httpServletRequest);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logErrorMessage("Http media type not acceptable", httpServletRequest, ex, httpServletRequest.getLocale());
        AppError error = new AppError(ErrorCodes.HTTP_MEDIA_TYPE_NOT_ACCEPTABLE, HttpStatus.UNSUPPORTED_MEDIA_TYPE, httpServletRequest);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logErrorMessage("Http message not readable", httpServletRequest, ex, httpServletRequest.getLocale());
        AppError error = new AppError(ErrorCodes.HTTP_MESSAGE_NOT_READABLE, HttpStatus.NOT_ACCEPTABLE, httpServletRequest);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(JsonParseException.class)
    public ResponseEntity<AppError> handleJsonParseException(HttpServletRequest request,
                                                             JsonParseException ex,
                                                             Locale locale) {
        logErrorMessage("Json Parse exception", request, ex, locale);
        AppError error = new AppError(ErrorCodes.JSON_PARSE_ERROR, HttpStatus.NOT_ACCEPTABLE, request);
        log.info("JsonParseException :: request.getMethod(): " + request.getMethod());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void logErrorMessage(String logLabel, HttpServletRequest request, Exception ex, Locale locale) {
        log.error("{}: - locale: {}, message: {}, stacktrace: {}", logLabel, locale, ex.getMessage(), getStackTrace(ex));
        log.info("{}: request method: {}, path: {}", ex.getClass().getSimpleName(), request.getMethod(), request.getRequestURI());
    }
}
