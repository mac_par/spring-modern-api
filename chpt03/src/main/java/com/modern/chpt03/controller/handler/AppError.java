package com.modern.chpt03.controller.handler;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

@Data
@NoArgsConstructor
public class AppError {
    private static final long serialVersionUID = 1L;
    private static final String NOT_AVAILABLE_MSG = "Not available";

    private String errorCode;
    private Integer status;
    private String message;
    private String url = NOT_AVAILABLE_MSG;
    private String reqMethod = NOT_AVAILABLE_MSG;

    public AppError(ErrorCodes status, HttpStatus httpStatus) {
        this.errorCode = status.getErrorCode();
        this.status = httpStatus.value();
        this.message = status.getErrorMessage();
    }

    public AppError(ErrorCodes status, int httpStatus) {
        this(status, HttpStatus.valueOf(httpStatus));
    }

    public AppError(ErrorCodes status, HttpStatus httpStatus, HttpServletRequest request) {
        this(status, httpStatus);
        this.url = request.getRequestURL().toString();
        this.reqMethod = request.getMethod();
    }

    public AppError(ErrorCodes status, int httpStatus, HttpServletRequest request) {
        this(status, HttpStatus.valueOf(httpStatus), request);
    }
}
