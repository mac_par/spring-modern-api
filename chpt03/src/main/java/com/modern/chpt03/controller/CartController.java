package com.modern.chpt03.controller;

import com.modernapi.model.dto.CartDto;
import com.modernapi.model.dto.ItemDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("carts/{customerId}")
public class CartController {
    @GetMapping
    ResponseEntity<CartDto> getCustomerCart(@PathVariable String customerId) {
        log.debug("Returning cart by customerId: {}", customerId);
        throw new RuntimeException("Method not implemented");
    }

    @DeleteMapping
    ResponseEntity<Void> deleteCartByCustomerId(@PathVariable String customerId) {
        log.debug("Removing cart by customer id {}", customerId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("items")
    ResponseEntity<List<ItemDto>> getItemsListByCustomerId(@PathVariable String customerId) {
        log.debug("Obtaining customer's items by customer id: {}", customerId);
        return ResponseEntity.ok(List.of());
    }

    @PostMapping("items")
    ResponseEntity<List<ItemDto>> addItemToCart(@PathVariable String customerId, @RequestBody ItemDto item) {
        log.debug("Adding item {} to cart identified by customer id: {}", item, customerId);
        return ResponseEntity.status(HttpStatus.CREATED).body(List.of(item));
    }

    @PutMapping("items")
    ResponseEntity<List<ItemDto>> addOrReplaceItemInCart(@PathVariable String customerId, @RequestBody ItemDto item) {
        log.debug("Adding or replacing carts item: {} for customer id: {}", item, customerId);
        return ResponseEntity.status(HttpStatus.CREATED).body(List.of(item));
    }

    @GetMapping("items/{itemId}")
    ResponseEntity<List<ItemDto>> getItemsListByCustomerId(@PathVariable Integer itemId, @PathVariable String customerId) {
        log.debug("Obtaining item {} from customer's cart by customer id: {}", itemId, customerId);
        return ResponseEntity.ok(List.of());
    }

    @DeleteMapping("items/{itemId}")
    ResponseEntity<Void> removeItemsFromCartByCustomerId(@PathVariable Integer itemId, @PathVariable String customerId) {
        log.debug("Removing item id {} from customer's cart by customer id: {}", itemId, customerId);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
