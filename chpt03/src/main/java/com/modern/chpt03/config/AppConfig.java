package com.modern.chpt03.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.modern.chpt03"})
@Configuration
public class AppConfig {

}
